<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;



class ProductsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'slug' => $this->slug,
            'price' => $this->price,
            'inventory' => $this->inventory,
            'shipping' => $this->shipping,            
            'description' => $this->description,
            'status' => $this->status,
            /* 'href' => [
               'link' => route('products.show',$this->id)
            ] */
        ];
    }
}
