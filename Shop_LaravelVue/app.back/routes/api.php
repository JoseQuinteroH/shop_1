<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Resources\ProductsResource as ProductsResource;
use App\Products;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::post('/register', 'RegisterController@register');
Route::post('/login', 'LoginController@login');
Route::post('/logout', 'LoginController@logout');
//Route::get('/user', 'UserController@index');
//Route::post('/products', 'ProductsController@index');

Route::post('/products', function () {
    return ProductsResource::collection(Products::all());
});

/*Route::apiResource('/products','ProductsController');
Route::group(['prefix' => 'products'],function(){
  Route::apiResource('/{product}/reviews','ReviewController');
});*/