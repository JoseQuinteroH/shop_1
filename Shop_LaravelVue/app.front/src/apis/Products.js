import Api from "./Api";

export default {
  async getProducts() {
    return Api.post("/products");
  },

  /* async login(form) {
    await Csrf.getCookie();

    return Api.post("/login", form);
  },

  async logout() {
    await Csrf.getCookie();

    return Api.post("/logout");
  }, */
};
