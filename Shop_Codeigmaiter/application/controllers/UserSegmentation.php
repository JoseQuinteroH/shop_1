<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserSegmentation extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		
		$this->load->model('M_usersegmentation');
	}

	public function index() {
		$data['userdata'] 	= $this->userdata;
		$data['dataUserSegmentation'] 	= $this->M_usersegmentation->select_all();

		$data['page'] 		= "User Segmentation";
		$data['titulo'] 		= "Data User Segmentation";
		$data['descripcion'] 	= "Manage Data User Segmentation";

		$data['modal_user_segmentation'] = show_my_modal('modals/modal_user_segmentation', 'usersegmentation', $data);

		$this->template->views('usersegmentation/home', $data);
	}

	

	public function getListData() {
		$data['datagetListData'] = $this->M_usersegmentation->select_all();
		$this->load->view('usersegmentation/list_data', $data);
	}

	public function addUserSegmentation() {
		
		$this->form_validation->set_rules('username', 'User Name', 'trim|required');
		$this->form_validation->set_rules('password', 'PassWord', 'trim|required');
		$this->form_validation->set_rules('namecompany', 'Name Company', 'trim|required');
		
		$data = $this->input->post();
		
		if ($this->form_validation->run() == TRUE){		
			$data["password_"] = $this->M_usersegmentation->encrypt_decrypt('encrypt', $data["password"]);
			$data["password"] = $this->M_usersegmentation->generateHash($data["password"]);			
			$result = $this->M_usersegmentation->insert($data,"userpie_users");			

			if ($result > 0) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Success', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Success - Not Change!!', '20px');
			}
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}

	public function update() {
		$data['userdata'] 	= $this->userdata;
		$id 				= trim($_POST['id']);
		$data['dataUserSegmentation'] 	= $this->M_usersegmentation->select_by_id($id);
		$data['dataUserSegmentation']->password_ = $this->M_usersegmentation->encrypt_decrypt('decrypt', $data['dataUserSegmentation']->password_);
		
		echo show_my_modal('modals/modal_update_user_segmentation', 'update-usersegmentation', $data);
	}

	public function updateUserSegmentation() {
		$this->form_validation->set_rules('username', 'User Name', 'trim|required');
		$this->form_validation->set_rules('password', 'PassWord', 'trim|required');		
		$this->form_validation->set_rules('namecompany', 'Name Company', 'trim|required');

		$data 	= $this->input->post();
		if ($this->form_validation->run() == TRUE) {
			$data["password_"] = $this->M_usersegmentation->encrypt_decrypt('encrypt', $data["password"]);
			$data["password"] = $this->M_usersegmentation->generateHash($data["password"]);
			$result = $result = $this->M_usersegmentation->update($data, array("user_id"=>$data["user_id"]));

			if ($result > 0) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Success', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Success - Not Change!!', '20px');
			}
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}	
}

/* End of file UserSegmentation.php */
/* Location: ./application/controllers/UserSegmentation.php */