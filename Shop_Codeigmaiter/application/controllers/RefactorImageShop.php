<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RefactorImageShop extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		
		$this->load->model('M_refactorimageshop');
	}

	public function index() {
		$data['userdata'] 	= $this->userdata;
		$data['dataRefactorImageShop'] 	= $this->M_refactorimageshop->select_all();

		$data['page'] 		= "Rafactor Image Shop";
		$data['titulo'] 		= "Data Refactor Image Shop";
		$data['descripcion'] 	= "Manage Data Refactor Image Shop";

		$data['modal_refactor_image_shop'] = show_my_modal('modals/modal_refactor_image_shop', 'refactorimageshop', $data);

		$this->template->views('refactorimageshop/home', $data);
	}	

	public function uploadImageShop(){
		//API URL	
		$data = json_encode( 
			array("data"=>array(
									"pImgen"=>$_REQUEST["data"]["pImgen"],
									"product_id"=>$_REQUEST["data"]["product_id"],
									"sku"=>$_REQUEST["data"]["sku"],
									"type"=>$_REQUEST["data"]["type"],
								)
				)							
		);                                                                 
		$data_string = json_encode($data);                                                                                   
																														
		$ch = curl_init($url);                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data_string))                                                                       
		);                                                                                                                   
																															
		$result = curl_exec($ch);
		var_dump($result);
	}

	public function uploadImageShop__(){	
		/* var_dump($_REQUEST["urlshop"]);
		var_dump($_REQUEST["data"]["pImgen"]);*/		
		$data = json_encode( 
								array("data"=>array(
														"pImgen"=>$_REQUEST["data"]["pImgen"],
														"product_id"=>$_REQUEST["data"]["product_id"],
														"sku"=>$_REQUEST["data"]["sku"],
														"type"=>$_REQUEST["data"]["type"],
													)
									)							
							);
		//$dataJson = json_decode($_REQUEST);	
        /* API URL */
        $url = $_REQUEST["urlshop"]."api-image.php";
		
        /* Init cURL resource */
        $ch = curl_init($url);
   
        /* Array Parameter Data */
        //$data["data"] = $_REQUEST["data"];
   
        /* pass encoded JSON string to the POST fields */
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            
		/* set the content type json */
		$headers = array(
			'Accept: application/json',
			'Content-Type: application/json',		
		);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            
        /* set return type json */
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            
        /* execute request */
        $result = curl_exec($ch);
             
        /* close cURL resource */
		curl_close($ch);
		
		echo $result;
	}

	public function getListData() {
		$result = file_get_contents($_REQUEST['data']."/api-image.php");
		$data['datagetListData'] = json_decode($result);
		$this->load->view('refactorimageshop/list_data', $data);
	}

	public function addRefactorImageShop() {
		
		$this->form_validation->set_rules('username', 'User Name', 'trim|required');
		$this->form_validation->set_rules('password', 'PassWord', 'trim|required');
		$this->form_validation->set_rules('namecompany', 'Name Company', 'trim|required');
		
		$data = $this->input->post();
		
		if ($this->form_validation->run() == TRUE){		
			$data["password_"] = $this->M_refactorimageshop->encrypt_decrypt('encrypt', $data["password"]);
			$data["password"] = $this->M_refactorimageshop->generateHash($data["password"]);			
			$result = $this->M_refactorimageshop->insert($data,"userpie_users");			

			if ($result > 0) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Success', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Success - Not Change!!', '20px');
			}
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}

	public function update() {
		$data['userdata'] 	= $this->userdata;
		$id 				= trim($_POST['id']);
		$data['dataUserSegmentation'] 	= $this->M_refactorimageshop->select_by_id($id);
		$data['dataUserSegmentation']->password_ = $this->M_refactorimageshop->encrypt_decrypt('decrypt', $data['dataUserSegmentation']->password_);
		
		echo show_my_modal('modals/modal_update_refactor_image_shop', 'update-refactorimageshop', $data);
	}

	public function updateRefactorImageShop() {
		$this->form_validation->set_rules('username', 'User Name', 'trim|required');
		$this->form_validation->set_rules('password', 'PassWord', 'trim|required');		
		$this->form_validation->set_rules('namecompany', 'Name Company', 'trim|required');

		$data 	= $this->input->post();
		if ($this->form_validation->run() == TRUE) {
			$data["password_"] = $this->M_refactorimageshop->encrypt_decrypt('encrypt', $data["password"]);
			$data["password"] = $this->M_refactorimageshop->generateHash($data["password"]);
			$result = $result = $this->M_refactorimageshop->update($data, array("user_id"=>$data["user_id"]));

			if ($result > 0) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Success', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Success - Not Change!!', '20px');
			}
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}	

	public function export() {
		error_reporting(E_ALL);
    
		include_once './assets/phpexcel/Classes/PHPExcel.php';
		$objPHPExcel = new PHPExcel();

		$data = $this->M_kota->select_all();

		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->setActiveSheetIndex(0); 

		$objPHPExcel->getActiveSheet()->SetCellValue('A1', "ID"); 
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', "Nama Kota");

		$rowCount = 2;
		foreach($data as $value){
		    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $value->id); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $value->nama); 
		    $rowCount++; 
		} 

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
		$objWriter->save('./assets/excel/Data Kota.xlsx'); 

		$this->load->helper('download');
		force_download('./assets/excel/Data Kota.xlsx', NULL);
	}

	public function import() {
		$this->form_validation->set_rules('excel', 'File', 'trim|required');

		if ($_FILES['excel']['name'] == '') {
			$this->session->set_flashdata('msg', 'File harus diisi');
		} else {
			$config['upload_path'] = './assets/excel/';
			$config['allowed_types'] = 'xls|xlsx';
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('excel')){
				$error = array('error' => $this->upload->display_errors());
			}
			else{
				$data = $this->upload->data();
				
				error_reporting(E_ALL);
				date_default_timezone_set('Asia/Jakarta');

				include './assets/phpexcel/Classes/PHPExcel/IOFactory.php';

				$inputFileName = './assets/excel/' .$data['file_name'];
				$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
				$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

				$index = 0;
				foreach ($sheetData as $key => $value) {
					if ($key != 1) {
						$check = $this->M_kota->check_nama($value['B']);

						if ($check != 1) {
							$resultData[$index]['nama'] = ucwords($value['B']);
						}
					}
					$index++;
				}

				unlink('./assets/excel/' .$data['file_name']);

				if (count($resultData) != 0) {
					$result = $this->M_kota->insert_batch($resultData);
					if ($result > 0) {
						$this->session->set_flashdata('msg', show_succ_msg('Data Kota Berhasil diimport ke database'));
						redirect('Kota');
					}
				} else {
					$this->session->set_flashdata('msg', show_msg('Data Kota Gagal diimport ke database (Data Sudah terupdate)', 'warning', 'fa-warning'));
					redirect('Kota');
				}

			}
		}
	}
}

/* End of file refactorimageshop.php */
/* Location: ./application/controllers/refactorimageshop.php */