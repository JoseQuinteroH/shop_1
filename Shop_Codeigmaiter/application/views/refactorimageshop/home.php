<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="box">
  <div class="box-header">
    <div class="col-md-12">

      <div class="input-group form-group col-md-12">
        <span class="input-group-addon" id="sizing-addon2">
        <i class="glyphicon glyphicon-globe"></i>
        </span>
        <input type="text" class="form-control" placeholder="Url Shop"  id="urlshop" aria-describedby="sizing-addon2">
        <select id="select-urlshop" class="form-control changeshop select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
        <?php            
            foreach ($dataRefactorImageShop as $data) {
              ?>
              <option><?php echo $data->website; ?></option>              
              <?php              
            }
        ?>          
        </select>
        <button class="form-control btn btn-primary get-image"><i class="glyphicon glyphicon-plus-sign"></i> Get Image</button>
        
      </div>     
      
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table id="list-data" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>Url Image</th>
          <th>Product Id</th>
          <th>SKU</th>
          <th>Name</th>  
          <th style="text-align: center;">Action</th>
        </tr>
      </thead>
      <tbody id="data-refactorimageshop">
      
      </tbody>
    </table>
  </div>
</div>

<?php //echo $modal_user_segmentation; ?>

<div id="temsegmentation-modal">
  
</div>

