/*
 Navicat Premium Data Transfer

 Source Server         : 0000-localhost
 Source Server Type    : MySQL
 Source Server Version : 100413
 Source Host           : localhost:3306
 Source Schema         : adminexamen

 Target Server Type    : MySQL
 Target Server Version : 100413
 File Encoding         : 65001

 Date: 20/08/2020 17:05:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `foto` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin', 'user.png');
INSERT INTO `admin` VALUES (2, 'super', 'f4e404c7f815fc68e7ce8e3c2e61e347', 'Super', 'profil2.jpg');

-- ----------------------------
-- Table structure for userpie_users
-- ----------------------------
DROP TABLE IF EXISTS `userpie_users`;
CREATE TABLE `userpie_users`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `username_clean` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password_` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '',
  `email` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `website` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `activationtoken` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `last_activation_request` int(11) NOT NULL,
  `LostpasswordRequest` int(1) NOT NULL DEFAULT 0,
  `active` int(1) NOT NULL,
  `group_id` int(11) NOT NULL,
  `sign_up_date` int(11) NOT NULL,
  `last_sign_in` int(11) NOT NULL,
  `namecompany` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `emailcc` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `emailbcc` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sendemail` tinyint(11) NOT NULL DEFAULT 0,
  `datemaillast` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 171 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of userpie_users
-- ----------------------------
INSERT INTO `userpie_users` VALUES (5, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', 'a9b479fe5cc21ae30faadc6c7443ab83', 1376950814, 0, 1, 1, 1376950814, 1597166307, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (71, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', 'a9b479fe5cc21ae30faadc6c7443ab83', 1376950814, 0, 1, 1, 1376950814, 1574833415, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (72, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', 'a9b479fe5cc21ae30faadc6c7443ab83', 1376950814, 0, 1, 1, 0, 1579734083, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (73, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', 'a9b479fe5cc21ae30faadc6c7443ab83', 1376950814, 0, 1, 1, 0, 1597337650, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (74, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', 'a9b479fe5cc21ae30faadc6c7443ab83', 1376950814, 0, 1, 1, 0, 1573196957, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (75, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', 'a9b479fe5cc21ae30faadc6c7443ab83', 1376950814, 0, 1, 1, 0, 1573498432, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (76, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', 'a9b479fe5cc21ae30faadc6c7443ab83', 1376950814, 0, 1, 1, 0, 1578690716, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (77, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', 'a9b479fe5cc21ae30faadc6c7443ab83', 1376950814, 0, 1, 1, 0, 1581351149, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (78, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', '', 0, 0, 1, 1, 0, 1576716195, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (79, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', '', 0, 0, 1, 1, 0, 1577128119, NULL, NULL, NULL, 2, '2020-08-19 01:24:26');
INSERT INTO `userpie_users` VALUES (80, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', '', 0, 0, 1, 1, 0, 1577478260, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (81, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', '', 0, 0, 1, 1, 0, 1576884354, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (82, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', 'a9b479fe5cc21ae30faadc6c7443ab83', 0, 0, 1, 1, 0, 1580491738, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (83, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', 'a9b479fe5cc21ae30faadc6c7443ab83', 0, 0, 1, 1, 0, 1578376955, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (84, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', 'a9b479fe5cc21ae30faadc6c7443ab83', 0, 0, 1, 1, 0, 1583304584, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (85, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', 'a9b479fe5cc21ae30faadc6c7443ab83', 0, 0, 1, 1, 0, 1579721244, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (86, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', 'a9b479fe5cc21ae30faadc6c7443ab83', 1376950814, 0, 1, 1, 0, 1597184664, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (87, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', 'a9b479fe5cc21ae30faadc6c7443ab83', 1376950814, 0, 1, 1, 0, 1584680248, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (88, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', 'a9b479fe5cc21ae30faadc6c7443ab83', 0, 0, 1, 1, 0, 1583166991, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (89, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', 'a9b479fe5cc21ae30faadc6c7443ab83', 1376950814, 0, 1, 1, 0, 1597264295, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (90, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', 'a9b479fe5cc21ae30faadc6c7443ab83', 0, 0, 1, 1, 0, 1597261549, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (91, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', 'a9b479fe5cc21ae30faadc6c7443ab83', 0, 0, 1, 1, 0, 1592979499, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (92, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', 'a9b479fe5cc21ae30faadc6c7443ab83', 0, 0, 1, 1, 0, 1596645921, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (93, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', 'fc609ea6511923a2d86620ed5acc41a5701b28b68f779df6bc081d8a7691850b6', 0, 0, 1, 1, 0, 1597087921, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (94, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', 'a9b479fe5cc21ae30faadc6c7443ab83', 0, 0, 1, 1, 0, 1595272219, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (95, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', '', 0, 0, 1, 1, 0, 1597256383, '', '', '', 0, NULL);
INSERT INTO `userpie_users` VALUES (96, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', '', 0, 0, 1, 1, 0, 1596664230, NULL, NULL, NULL, 0, NULL);
INSERT INTO `userpie_users` VALUES (97, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', '', 0, 0, 1, 1, 0, 1597289750, '', '', '', 1, '2020-08-19 01:16:43');
INSERT INTO `userpie_users` VALUES (170, 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'test@email.com', 'http://www.test.com/', '', 0, 0, 1, 0, 0, 0, '', '', '', 2, '2020-08-19 01:14:48');

SET FOREIGN_KEY_CHECKS = 1;
