<script type="text/javascript">
	var MyTable = $('#list-data').dataTable({
		  "paging": true,
		  "lengthChange": true,
		  "searching": true,
		  "ordering": true,
		  "info": true,
		  "autoWidth": false
		});

	window.onload = function() {		
		getListDataUserSegmentation();
		<?php
			if ($this->session->flashdata('msg') != '') {
				echo "effect_msg();";
			}
		?>
	}

	function refresh() {
		MyTable = $('#list-data').dataTable();
	}

	function effect_msg_form() {
		// $('.form-msg').hide();
		$('.form-msg').show(1000);
		setTimeout(function() { $('.form-msg').fadeOut(1000); }, 3000);
	}

	function effect_msg() {
		// $('.msg').hide();
		$('.msg').show(1000);
		setTimeout(function() { $('.msg').fadeOut(1000); }, 3000);
	}
	
	//****************************************************************/
	function getListDataUserSegmentation() {
		$.get('<?php echo base_url('UserSegmentation/getListData'); ?>', function(data) {
			MyTable.fnDestroy();
			$('#data-usersegmentation').html(data);
			refresh();
		});
	}

	function getListDataRefactorImageShop(data) {
		$.post('<?php echo base_url('RefactorImageShop/getListData'); ?>', data, function(data) {
			MyTable.fnDestroy();
			$('#data-refactorimageshop').html(data);
			refresh();
		});
	}

	var id_usersegmentation;
	$(document).on("click", ".konfirmasiHapus-usersegmentation", function() {
		id_usersegmentation = $(this).attr("data-id");
	})
	$(document).on("click", ".hapus-dataUserSegmentation", function() {
		var id = id_usersegmentation;
		
		$.ajax({
			method: "POST",
			url: "<?php echo base_url('UserSegmentation/delete'); ?>",
			data: "id=" +id
		})
		.done(function(data) {
			$('#konfirmasiHapus').modal('hide');
			getListDataUserSegmentation();
			$('.msg').html(data);
			effect_msg();
		})
	})

	$(document).on("click", ".update-dataUserSegmentation", function() {
		var id = $(this).attr("data-id");
		
		$.ajax({
			method: "POST",
			url: "<?php echo base_url('UserSegmentation/update'); ?>",
			data: "id=" +id
		})
		.done(function(data) {
			$('#temsegmentation-modal').html(data);
			$('#update-usersegmentation').modal('show');			
		})
	})

	
	$(document).on("change", ".changeshop", function() {
		$("#urlshop").val($("#select-urlshop").val());
	})

	$(document).on("click", ".get-image", function() {
		var data = $("#urlshop").val();	

		$.ajax({
			method: "POST",
			url: "<?php echo base_url('RefactorImageShop/getListData'); ?>",
			data: "data=" +data
		})
		.done(function(data) {

			MyTable.fnDestroy();
			$('#data-refactorimageshop').html(data);
			refresh();
			
		})		
		e.preventDefault();		
	})	

	$(document).on("click", ".upload-dataRefactorImageShop", function() {
		var id = $(this).attr("data-id");
		var obj = id.split("-");
		var objData = {
						urlshop:$('#urlshop').val(),
						data:{
								pImgen:$('#'+id).val(), 
								product_id:obj[0],
								sku:obj[1],
								type:"uploadimage"
							 }
					  };
					  		
		$.ajax({
			method: "POST",
			url: "<?php echo base_url('RefactorImageShop/uploadImageShop'); ?>",
			data: /*JSON.stringify(*/objData/*)*/
		})
		.done(function(data) {
			/* var out = jQuery.parseJSON(data);

			getListDataUserSegmentation();
			if (out.status == 'form') {				
				$('.form-msg').html(out.msg);
				effect_msg_form();				
			} else {				
				$('.msg').html(out.msg);
				effect_msg();
			} */ 
		})		
		e.preventDefault();
		
	})

	$(document).on("click", ".email-dataUserSegmentation", function() {
		var id = $(this).attr("data-id");
		
		$.ajax({
			method: "POST",
			url: "<?php echo base_url('Email/send'); ?>",
			data: "id=" +id
		})
		.done(function(data) {
			var out = jQuery.parseJSON(data);

			getListDataUserSegmentation();
			if (out.status == 'form') {				
				$('.form-msg').html(out.msg);
				effect_msg_form();				
			} else {				
				$('.msg').html(out.msg);
				effect_msg();
			} 
		})		
		e.preventDefault();
	})

	$('#form-usersegmentation').submit(function(e) {
		var data = $(this).serialize();
        //console.log(data);
		$.ajax({
			method: 'POST',
			url: '<?php echo base_url('UserSegmentation/addUserSegmentation'); ?>',
			data: data
		})
		.done(function(data) {
			var out = jQuery.parseJSON(data);

			getListDataUserSegmentation();
			if (out.status == 'form') {				
				$('.form-msg').html(out.msg);
				effect_msg_form();				
			} else {					
				$('#usersegmentation').modal('hide');
				$('.msg').html(out.msg);
				effect_msg();
			} 
		})		
		e.preventDefault();
	});

	$(document).on('submit', '#form-update-usersegmentation', function(e){
		var data = $(this).serialize();

		$.ajax({
			method: 'POST',
			url: '<?php echo base_url('UserSegmentation/updateUserSegmentation'); ?>',
			data: data
		})
		.done(function(data) {
			var out = jQuery.parseJSON(data);

			getListDataUserSegmentation();
			if (out.status == 'form') {
				$('.form-msg').html(out.msg);
				effect_msg_form();
			} else {
				document.getElementById("form-update-usersegmentation").reset();
				$('#update-usersegmentation').modal('hide');
				$('.msg').html(out.msg);
				effect_msg();
			}
		})
		
		e.preventDefault();
	});

	$('#update-usersegmentation').on('hidden.bs.modal', function (e) {		
	  	//$('.form-msg').html('');	  
	}).on('shown.bs.modal', function (e) {		
		//$("#form-usersegmentation").get(0).reset();
	})
	//****************************************************************/

	$('.select2').select2()
	
</script>